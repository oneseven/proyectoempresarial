<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Usuario</title>
</head>
<body>
	<jsp:include page="MasterMenu.jsp"/>
	
	<table >
		<tr>
			<th>Id</th>
			<th>Nombre</th>
			<th>Apellido</th>
			<th>Usuario</th>
			<th>Estado</th>
			<th>TipoUsuario</th>
		</tr>
		
			<c:forEach items="${listas}" var="list">
				<tr>
					<td><c:out value="${list.getIdUsuario()}"/></td>
					<td><c:out value="${list.getEmpleado().getNombres()}"/></td>
					<td><c:out value="${list.getEmpleado().getApellidos()}"/></td>
					<td><c:out value="${list.getUsuario()}"/></td>
					<td><c:out value="${list.getEstado().getValor()}"/></td>					
					<td><c:out value="${list.getTipoUsuario().getValor()}"/></td>
				
				</tr>
			</c:forEach>
		
	</table>

</body>
</html>