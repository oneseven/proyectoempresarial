<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nuevo Proveedor</title>
</head>
<body>
	<jsp:include page="MasterMenu.jsp"/>
	
	<form:form method="POST" action="">
		<table>
			<tr>
				<td>RazonSocial:</td>
				<td><input name="RazonSocial" type="text"/>
			<tr />
			<tr>
				<td>Ruc:</td>
				<td><input name="RUC" type="text"/>
			<tr />
			<tr>
				<td>Direccion:</td>
				<td><input name="Direccion" type="text"/>
			<tr />
			<tr>
				<td>Telefono:</td>
				<td><input name="Telefono" type="text"/>
			<tr />
			<tr>
				<td>Email:</td>
				<td><input name="Email" type="text"/>
			<tr />
			<tr>
				<td>Estado:</td>
				<td><input name="Estado" type="text"/>
			<tr />
			<tr>
				<td>Fecha Registro:</td>
				<td><input name="FechaRegistro" type="text"/>
			<tr />
			<tr>
				<td>Usuario:</td>
				<td><input name="Usuario" type="text"/>
			<tr />
			<tr>
				<td>Fecha Modificacion:</td>
				<td><input name="FechaModificacion" type="text"/>
			<tr />
			<tr>
				<td colspan="2">
					<input type="submit" value="Registrar" name="btnRegistrar" />
					<input type="reset" value="Limpiar" />
				</td>
			</tr>
		</table>
	</form:form>
</body>
</html>