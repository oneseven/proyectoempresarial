package com.datos.comun;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.datos.seguridad.SEG_UsuarioDL;
import com.entidades.comun.CMM_EstadoEL;
import com.entidades.comun.CMM_PersonaEL;
import com.entidades.comun.CMM_ProveedoresEL;
import com.entidades.seguridad.SEG_UsuarioEL;

public class SEG_ProveedoresDL {
	// Singleton
	public static SEG_ProveedoresDL _Instancia;

	private SEG_ProveedoresDL() {
	};

	public static SEG_ProveedoresDL Instancia() {
		if (_Instancia == null) {
			_Instancia = new SEG_ProveedoresDL();
		}
		return _Instancia;
	}

	// endSingleton
	public ArrayList<CMM_ProveedoresEL> ListarProveedores() throws Exception {
		Connection cn = null;
		ArrayList<CMM_ProveedoresEL> lista = new ArrayList<CMM_ProveedoresEL>();
		try {
			cn = Conexion.Instancia().Conectar();
			CallableStatement cst = cn.prepareCall("{call spListarProveedor(?)}");
			cst.setInt(1, 0);
			ResultSet rs = cst.executeQuery();
			while (rs.next()) {
				CMM_ProveedoresEL p = new CMM_ProveedoresEL();
				p.setIdProveedor(rs.getInt("Provee_Id"));
				p.setRazonSocial(rs.getString("Provee_RazonSocial"));
				p.setRUC(rs.getString("Provee_RUC").toString());
				p.setDireccion(rs.getString("Provee_Direccion"));
				p.setTelefono(rs.getString("Provee_Telefono"));
				p.setEmail(rs.getString("Provee_Email"));
				CMM_EstadoEL e=new CMM_EstadoEL();
				e.setValor(rs.getString("Estad_Valor"));
				p.setEstado(e);				
				lista.add(p);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
		}
		return lista;
	}

	public boolean AgregarProveedores(CMM_ProveedoresEL proveedores) throws Exception {
		Connection con = null;
		CallableStatement cstm = null;
		try {
			con = Conexion.Instancia().Conectar();
			String sql = "{CALL spAgregarProveedor(?,?,?,?,?,?,?,?,?)}";
			cstm = con.prepareCall(sql);
			cstm.setString(1, proveedores.getRazonSocial());
			cstm.setString(2, proveedores.getRUC());
			cstm.setString(3, proveedores.getDireccion());
			cstm.setString(4, proveedores.getTelefono());
			cstm.setString(5, proveedores.getEmail());
			cstm.setInt(6, proveedores.getEstado().getIdEstado());
			cstm.setDate(7, proveedores.getFechaRegistro());
			cstm.setInt(8, proveedores.getUsuario().getIdUsuario());
			cstm.setDate(9, proveedores.getFechaModificacion());
			cstm.execute();
			System.out.println("Exito consulta AgregarProveedores");
			return true;
		} catch (Exception e) {
			System.out.println("Error consulta AgregarProveedores");
			e.printStackTrace();
			return false;
		} finally {
			cstm.close();
			con.close();
		}
	}
/*
	public boolean EditarProveedores(CMM_ProveedoresEL p) throws Exception {
		// Iniciar variables
		Connection conn = null;
		CallableStatement cstm = null;
		try {
			conn = Conexion.Instancia().Conectar();
			String sql = "{CALL SP_CMM_ModificarProveedores(?,?,?,?)}";
			cstm = conn.prepareCall(sql);

			cstm.setInt(1, p.getIdProveedor());
			cstm.setString(2, p.getRazonSocial());
			cstm.setString(3, p.getRuc());
			cstm.setString(4, p.getDireccion());

			cstm.executeUpdate();
			System.out.println("Exito SEG_ProveedoresDL EditarProveedores");
			return true;
		} catch (Exception e) {
			System.out.println("Error SEG_ProveedoresDL EditarProveedores" + e.getMessage());
			e.printStackTrace();
			return false;

		} finally {
			cstm.close();
			conn.close();
		}
	}
	
	public CMM_ProveedoresEL BuscarProveedores(int id) throws Exception {
		Connection cn = null;
		CMM_ProveedoresEL p = null;
		try {
			cn = Conexion.Instancia().Conectar();
			CallableStatement cst = cn.prepareCall("{call spListarProveedores(?)}");
			cst.setInt(1, id);
			ResultSet rs = cst.executeQuery();
			while (rs.next()) {
				p = new CMM_ProveedoresEL();
				p.setIdProveedor(rs.getInt("IdProveedor"));
				p.setRazonSocial(rs.getString("RazonSocial"));
				p.setRuc(rs.getString("Ruc"));
				p.setDireccion(rs.getString("Direccion"));
			}
		} catch (Exception e) {
			throw e;
		} finally {
			cn.close();
		}
		return p;
	}
	//SP_CMM_EliminarProveedores
	public boolean EliminarProveedores(int id) throws Exception {
	// Iniciar variables
		Connection conn = null;
		CallableStatement cstm = null;
		try {
			conn = Conexion.Instancia().Conectar();
			String sql = "{CALL SP_CMM_EliminarProveedores(?)}";
			cstm = conn.prepareCall(sql);
			cstm.setInt(1, id);
			cstm.executeUpdate();
			System.out.println("Exito SEG_ProveedoresDL EliminarProveedores");
			return true;
		} catch (Exception e) {
			System.out.println("Error SEG_ProveedoresDL EliminarProveedores" + e.getMessage());
			e.printStackTrace();
			return false;

		} finally {
			cstm.close();
			conn.close();
		}
	}
*/
}
