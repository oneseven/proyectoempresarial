package com.entidades.comun;

public class CMM_TipoUsuarioEL {

	private int IdTipoUsuario;
	private String valor;
	
	public int getIdTipoUsuario() {
		return IdTipoUsuario;
	}
	public void setIdTipoUsuario(int idTipoUsuario) {
		IdTipoUsuario = idTipoUsuario;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
}
