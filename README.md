# **# README #** #

**Pequeño proyecto utilizando arquitectura de capas y spring**

**### REPOSITORIO BITBUCKET ###**
*https://bitbucket.org/oneseven/proyectoempresarial*

**### FUNCIONES ###**

* Iniciar Session
* Listar
* Agregar
* Editar
* Eliminar

**### ARCHIVOS ###**

* Capa Datos
* Capa Entidades
* Capa Negocios
* Capa Presentacion
* Script de la Base de Datos

**### CREDITOS ###**

* Admin - Manuel Guarniz - OnesevenDevelopment
* Facebook: https://www.facebook.com/manuel.guarniz.39
* Twitter: https://twitter.com/cruzemg95 (@cruzemg95)