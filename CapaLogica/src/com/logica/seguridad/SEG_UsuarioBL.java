package com.logica.seguridad;

import java.util.ArrayList;

import com.datos.seguridad.SEG_UsuarioDL;
import com.entidades.seguridad.SEG_UsuarioEL;

public class SEG_UsuarioBL {
	// Singleton
	public static SEG_UsuarioBL _Instancia;

	private SEG_UsuarioBL() {
	};

	public static SEG_UsuarioBL Instancia() {
		if (_Instancia == null) {
			_Instancia = new SEG_UsuarioBL();
		}
		return _Instancia;
	}
	// endSingleton

	public SEG_UsuarioEL VerificarAcceso(String _Usuario, String _Passw) throws Exception {
		try {
			SEG_UsuarioEL u = SEG_UsuarioDL.Instancia().VerificarAcceso(_Usuario, _Passw);
			if (u == null) {
				throw new Exception("Usuario o Contraseņa incorrecta");
			} else {
				if (u.getEstado().getIdEstado()!=1) {
					throw new Exception("Usuario ha sido dado de baja");
				}
				java.util.Date utilDate = new java.util.Date();
				java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
				/*if (u.getFechaPermisoHasta().getTime() <= sql.getTime()) {
					throw new Exception("Usuario sin Permisos");
				}*/
			}
			return u;
		} catch (Exception e) {
			throw e;
		}
	}
	

	public ArrayList<SEG_UsuarioEL> ListarUsuarios() throws Exception {
		try {
			return SEG_UsuarioDL.Instancia().ListarUsuario();
		} catch (Exception e) {
			throw e;
		}

	}


}
