package com.logica.comun;

import java.util.ArrayList;

import com.datos.comun.SEG_ProveedoresDL;
import com.entidades.comun.CMM_ProveedoresEL;

public class CMM_ProveedoresBL {
	public static CMM_ProveedoresBL _Instancia;

	private CMM_ProveedoresBL() {
	};

	public static CMM_ProveedoresBL Instancia() {
		if (_Instancia == null) {
			_Instancia = new CMM_ProveedoresBL();
		}
		return _Instancia;
	}

	public ArrayList<CMM_ProveedoresEL> ListarProveedores() throws Exception {
		try {
			return SEG_ProveedoresDL.Instancia().ListarProveedores();
		} catch (Exception e) {
			throw e;
		}

	}

	
	public boolean AgregarProveedores(CMM_ProveedoresEL proveedores) throws Exception {
		boolean var = false;
		try {
			var = SEG_ProveedoresDL.Instancia().AgregarProveedores(proveedores);
			if (var) {
				System.out.println("Exito AgregarProveedoresBL");
			}
			return var;
		} catch (Exception e) {
			System.out.println("Error AgregarProveedoresBL" + e.getMessage());
			throw e;

		}
	}
/*
	public boolean EditarProveedores(CMM_ProveedoresEL p) throws Exception {

		boolean var = false;

		try {
			var = SEG_ProveedoresDL.Instancia().EditarProveedores(p);
			if (var) {
				System.out.println("Exito CMM_ProveedoresBL EditarProveedores");
			}
			return var;
		} catch (Exception e) {
			System.out.println("Error CMM_ProveedoresBL  EditarProveedores" + e.getMessage());
			throw e;
		}
	}

	public CMM_ProveedoresEL BuscarProveedores(int id) throws Exception {
		try {
			CMM_ProveedoresEL p = SEG_ProveedoresDL.Instancia().BuscarProveedores(id);
			System.out.println("Exito CMM_ProveedoresBL BuscarProveedores");
			return p;
		} catch (Exception e) {
			System.out.println("Error CMM_ProveedoresBL  BuscarProveedores" + e.getMessage());
			throw e;
		}
	}
	public boolean EliminarProveedores(int id) throws Exception {
		boolean var = false;

		try {
			var = SEG_ProveedoresDL.Instancia().EliminarProveedores(id);
			if (var) {
				System.out.println("Exito CMM_ProveedoresBL EliminarProveedores");
			}
			return var;
		} catch (Exception e) {
			System.out.println("Error CMM_ProveedoresBL  EliminarProveedores" + e.getMessage());
			throw e;
		}
	}

*/
}
