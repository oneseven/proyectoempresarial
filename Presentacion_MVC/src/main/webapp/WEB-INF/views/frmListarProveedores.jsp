<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>
	<section>
		<jsp:include page="MasterMenu.jsp" flush="true" />
	</section>
	
	<section>
		<a href="${pageContext.request.contextPath}/NuevoProveedor">Nuevo</a>
		<table >
		
			<tr>
				<th>ID</th>
				<th>Razon Social</th>
				<th>RUC</th>
				<th>Direcci�n</th>
				<th>Telefono</th>
				<th>Email</th>
				<th>Estado</th>
				<th colspan="2"></th>
			</tr>
		
			
				<c:forEach items="${listas}" var="list">
					<tr>
						<td><c:out value="${list.getIdProveedor()}" /></td>
						<td><c:out value="${list.getRazonSocial()}" /></td>
						<td><c:out value="${list.getRUC()}" /></td>
						<td><c:out value="${list.getDireccion()}" /></td>
						<td><c:out value="${list.getTelefono()}" /></td>
						<td><c:out value="${list.getEmail()}" /></td>
						<td><c:out value="${list.getEstado().getValor()}" /></td>	
						<td><a href="#">Editar</a></td>
						<td><a href="${pageContext.request.contextPath}/EliminarProv/${list.idProveedor}" onclick="
							return confirm('�Estas seguro de eliminar?')">Eliminar</a></td>
					</tr>
				</c:forEach>
			
		</table>
	
	</section>
	

</body>
</html>